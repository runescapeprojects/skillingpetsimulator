"""
Old School RuneScape pet chance calculator
Created by RenderScape
This has been created using public knowledge from the OSRS wiki
It is not affiliated with Jagex in ANY WAY
"""
level_to_xp = {1: 0,
               2: 83,
               3: 91,
               4: 102,
               5: 112,
               6: 124,
               7: 138,
               8: 151,
               9: 168,
               10: 185,
               11: 204,
               12: 226,
               13: 249,
               14: 274,
               15: 304,
               16: 335,
               17: 369,
               18: 408,
               19: 450,
               20: 497,
               21: 548,
               22: 606,
               23: 667,
               24: 737,
               25: 814,
               26: 898,
               27: 990,
               28: 1094,
               29: 1207,
               30: 1332,
               31: 1470,
               32: 1623,
               33: 1791,
               34: 1977,
               35: 2182,
               36: 2409,
               37: 2658,
               38: 2935,
               39: 3240,
               40: 3576,
               41: 3947,
               42: 4358,
               43: 4810,
               44: 5310,
               45: 5863,
               46: 6471,
               47: 7144,
               48: 7887,
               49: 8707,
               50: 9612,
               51: 10612,
               52: 11715,
               53: 12934,
               54: 14278,
               55: 15764,
               56: 17404,
               57: 19214,
               58: 21212,
               59: 23420,
               60: 25856,
               61: 28546,
               62: 31516,
               63: 34795,
               64: 38416,
               65: 42413,
               66: 46826,
               67: 51699,
               68: 57079,
               69: 63019,
               70: 69576,
               71: 76818,
               72: 84812,
               73: 93638,
               74: 103383,
               75: 114143,
               76: 126022,
               77: 139138,
               78: 153619,
               79: 169608,
               80: 187260,
               81: 206750,
               82: 228269,
               83: 252027,
               84: 278259,
               85: 307221,
               86: 339198,
               87: 374502,
               88: 413482,
               89: 456519,
               90: 504037,
               91: 556499,
               92: 614422,
               93: 678376,
               94: 748985,
               95: 826944,
               96: 913019,
               97: 1008052,
               98: 1112977,
               99: 1228825,
               100: 186965569}


class Player:
    def __init__(self, start_level):
        self.current_level = start_level
        self.chance_not_get_final = 0
        self.carryover_xp = 0

    def perform_grind(self, end_level, xp_per_action, pet_base_chance, static):
        action_count = 0
        xp_earned = 0

        while self.current_level < end_level:
            xp_to_next_level = level_to_xp[self.current_level + 1] - self.carryover_xp

            if static:
                chance_not_get_action = 1 - (1 / pet_base_chance)
            else:
                chance_not_get_action = 1 - (1 / (pet_base_chance - (25 * self.current_level)))

            print(f'Chance not to get in this action: {chance_not_get_action}')

            while xp_to_next_level > 0:
                #print(f'Current Level: {self.current_level}, xp till next level: {xp_to_next_level}')
                # perform 1 action
                action_count += 1
                xp_to_next_level -= xp_per_action
                xp_earned += xp_per_action

                if self.chance_not_get_final == 0:
                    self.chance_not_get_final = chance_not_get_action
                else:
                    self.chance_not_get_final *= chance_not_get_action
            else:
                self.carryover_xp = abs(xp_to_next_level)
                self.current_level += 1

        chance_get = 1 - self.chance_not_get_final
        chance_get_percent = str(chance_get * 100)
        print(f'After {action_count} actions, you had a {chance_get_percent}% chance of getting at least one pet. '
              f'You earned {xp_earned} experience')


start_level = int(input('insert start level: '))
player = Player(start_level)

print("Let's grind for a pet!\n\n")
done = False

while not done:
    goal_level = int(input(f'Current level is: {player.current_level}, insert end level: '))
    pet_base_chance = input('insert pet base chance: ')
    if 'static' in pet_base_chance:
        static = True
        pet_base_chance = int(pet_base_chance.split(' ')[0])
    else:
        static = False
        pet_base_chance = int(pet_base_chance)
    action_xp = float(input('insert action experience: '))
    player.perform_grind(goal_level, action_xp, pet_base_chance, static)
    again_answer = input(f'Current level is: {player.current_level}, Do you want to do another grind?')
    if again_answer == 'n':
        done = True
    else:
        done = False


